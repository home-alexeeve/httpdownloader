# README #
### What is this repository for? ###

This app allows downloading files from the internet, concurrently (configurable) and with a specified download speed (configurable).

+ Input parameters:
1. f - path to file with download URLs
2. o - path to directory where downloaded files will be stored
3. n - number of concurrent threads  
4. l - max limit of download speed in bytes per second
  
File with download urls has following format:
[HTTP file URL][space character][local file name for downloaded file]
  
Example of file
`http://example.com/file.txt localFileName.txt http://example.com/file2.pdf localFileName2.pdf`
  
### How do I get set up? ###

Example of utility invocation:
`httpDownloader.exe -f urls.txt -o downloads -n 3 -l 2000`