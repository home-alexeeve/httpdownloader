#addin "nuget:?package=Newtonsoft.Json"

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

JToken ReplacePath<T>(JToken root, string path, T newValue)
{
    if (root == null || path == null)
        throw new ArgumentNullException();

    foreach (var value in root.SelectTokens(path).ToList())
    {
        if (value == root)
            root = JToken.FromObject(newValue);
        else
            value.Replace(JToken.FromObject(newValue));
    }

    return root;
}

string ReplacePath<T>(string jsonString, string path, T newValue)
{
    return ReplacePath(JToken.Parse(jsonString),path, newValue).ToString();
}
