#tool "nuget:?package=xunit.runner.console"
#tool "nuget:?package=OpenCover&version=4.6.519"
#tool "nuget:?package=ReportGenerator&version=2.5.2.0"


using System;
using System.Xml;

///////////////////////////////////////////////////////////////////////////////
// VARIABLES
///////////////////////////////////////////////////////////////////////////////

var buildOutputDir = Directory("../output");
var testResultsDir = MakeAbsolute(buildOutputDir + Directory("test-results"));
var testProjects = new List<string> {
    "HttpFileDownloader.Tests"
};


// ARGUMENTS
///////////////////////////////////////////////////////////////////////////////

var rebuild = Argument("rebuild", true);
var target = Argument("target", "Default");
var buildNumber = Argument("buildNumber", 1);
var configuration = Argument("configuration", "Debug");
var dotNetCoreRestoreVerbositySetting = Argument<DotNetCoreVerbosity>("dotNetCoreRestoreVerbosity", DotNetCoreVerbosity.Minimal);

///////////////////////////////////////////////////////////////////////////////
// TASKS
///////////////////////////////////////////////////////////////////////////////


Task("Clean")
    .Does(() =>
    {
        CleanDirectories("../**/bin/"+ configuration);
    });



Task("Build")
    .Does(() =>
{
    var argValue = string.Empty;
    argValue = buildNumber > 0 ? argValue = "/p:BuildNumber=" + buildNumber : "/m" ;

    var versionArgument  = new Func<ProcessArgumentBuilder, ProcessArgumentBuilder> (args => args.Append(argValue)); 

    var projects = GetFiles("../src/**/*.csproj");
    projects.Add(GetFiles("../test/**/*.csproj"));

    foreach (var project in projects){
            DotNetCoreBuild(project.FullPath, new DotNetCoreBuildSettings {
                Configuration = configuration,
                 MSBuildSettings = new DotNetCoreMSBuildSettings {
                    ArgumentCustomization = versionArgument
                }
           });
    }

    CreateDirectory("../output/test-results");
});

Task("RunTests")
    .IsDependentOn("Clean")
    .IsDependentOn("Build")
    .Does(() =>
{
    var exceptions = new List<Exception>();
    var rootTestDirectory = MakeAbsolute(Directory("../test")).ToString();
    
    var runTest = new Action<string>(testProjName =>
    {
        try
        {
            DotNetCoreTest(rootTestDirectory + "/" + testProjName, new DotNetCoreTestSettings {
                Configuration = configuration,
                WorkingDirectory = MakeAbsolute(Directory(rootTestDirectory + "/" + testProjName)),
                ArgumentCustomization = args =>
                    args.Append("--logger \"trx;LogFileName=" + testResultsDir + "/" + testProjName +".XUnit.xml")
            });
        }
        catch(Exception ex) {
            Console.WriteLine(ex);
            exceptions.Add(ex);
        }
    });

    foreach(var testProject in testProjects)
    {
        runTest(testProject);
    }

    if (exceptions.Count > 0)
    {
        throw exceptions.First();
    }
});

Task("RunTestsWithCoverage")
   .IsDependentOn("Clean")
   .IsDependentOn("Build")
   .Does(() =>
{
   var rootTestDirectory = MakeAbsolute(Directory("../test")).ToString();

   // Using Partial Application to create two custom actions
   Func<string, Action<ICakeContext>> makeTestAction = testProjName => ctx => {
       ctx.DotNetCoreTest(rootTestDirectory + "/" + testProjName, new DotNetCoreTestSettings {
           Configuration = configuration,
           WorkingDirectory = MakeAbsolute(Directory(rootTestDirectory + "/" + testProjName)),
        ArgumentCustomization = args =>
                args.Append("--logger \"trx;LogFileName=" + testResultsDir + "/" + testProjName +".XUnit.xml")
       });
   };

   Func<string, OpenCoverSettings> makeSettings = testProjName => {
        return new OpenCoverSettings {
            WorkingDirectory = Directory(rootTestDirectory + "/" + testProjName),
            ArgumentCustomization = args =>
                args.Append("-skipautoprops")
                .Append("-oldStyle")
                .Append("-hideskipped:All")
                .Append(@"-searchdirs:""{0}""", String.Join(";", GetDirectories("../test/**/bin/" + configuration + "/*")))
        }.WithFilter("+[HttpFileDownloader]*")
        .WithFilter("-[*Tests*]*")
        .WithFilter("-[xunit*]*")
        .WithFilter("-[*FluentAssertions*]*");
   };

   Func<string, FilePath> coverageFile = testProjName => File(string.Format("{0}/{1}-CodeCoverage.xml", testResultsDir, testProjName));

   foreach(var testProject in testProjects)
   {
        OpenCover(
            makeTestAction(testProject),
            coverageFile(testProject),
            makeSettings(testProject));
   }
});

Task("CopyPackages")
    .IsDependentOn("Build")
    .Does(() =>
{
    var files = GetFiles("./src/**/*.nupkg");
    CopyFiles(files, "./artifacts");

});

Task("GenerateCodeCoverageReport")
    .IsDependentOn("RunTestsWithCoverage")
    .Does(() =>
{
    Func<string, FilePath> coverageFile = testName =>
        File(string.Format("{0}/{1}-CodeCoverage.xml", testResultsDir, testName));
    var coverageFiles = new List<FilePath>();

    foreach(var testProject in testProjects)
    {
        coverageFiles.Add(coverageFile(testProject));
    }
    ReportGenerator(coverageFiles,testResultsDir + "/codeCoverageReport");
});

    
Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Build")
    .IsDependentOn("RunTestsWithCoverage")
    .IsDependentOn("GenerateCodeCoverageReport");

RunTarget(target);
