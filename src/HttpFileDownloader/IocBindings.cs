using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace HttpFileDownloader {
    public class IocBindings {
        public static ServiceCollection Configure() {
            var services = new ServiceCollection();

            var loggerFactory = new LoggerFactory().AddNLog();
            loggerFactory.ConfigureNLog("nlog.config");

            services.AddSingleton<IBulkFileDownloader, BulkFileDownloader>()
                .AddSingleton<IFileSystemHandler, FileSystemHandler>()
                .AddSingleton<IHttpClientFactory, HttpClientInstance>()
                .AddSingleton<IConfigBuilder, BulkDownloaderConfig>()
                .AddSingleton<ICliApp, BulkDownloadCliApp>()
                .AddSingleton<IHttpClientService, HttpClientService>()
                .AddSingleton<IFileDownloader, FileDownloader>()
                .AddSingleton(loggerFactory)
                .AddLogging();

            return services;
        }
    }
}