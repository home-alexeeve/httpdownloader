using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpFileDownloader {
    public interface IHttpClientService {
        Task<Stream> DownloadFile(string absoluteUri);
    }

    public class HttpClientService : IHttpClientService {
        private readonly HttpClient httpClient;

        public HttpClientService(HttpClient httpClient) {
            this.httpClient = httpClient;
        }

        public HttpClient HttpClient => this.httpClient;

        public async Task<Stream> DownloadFile(string absoluteUri) {
            return await this.httpClient.GetAsync(absoluteUri, null);
        }

    }
}