using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace HttpFileDownloader {
    public interface IBulkFileDownloader {
        void BulkDownload(BulkDownloaderConfig config);
    }

    public class BulkFileDownloader : IBulkFileDownloader {
        private int MaxThreadCount {
            get;
            set;
        }

        private readonly IFileSystemHandler fileSystemHandler;
        private readonly IFileDownloader fileDownloader;

        private readonly ILogger logger;

        public BulkFileDownloader(
            IFileSystemHandler fileSystemHandler,
            IFileDownloader fileDownloader,
            ILoggerFactory loggerFactory) {
            this.logger = loggerFactory.CreateLogger(this.GetType().Name);
            this.fileDownloader = fileDownloader;
            this.fileSystemHandler = fileSystemHandler;
        }

        public void BulkDownload(BulkDownloaderConfig config) {
            MaxThreadCount = config.ThreadNumber;
            var urls = fileSystemHandler.ReadTxt(config.PathToFile);

            Parallel.For(0, urls.Count(), new ParallelOptions { MaxDegreeOfParallelism = config.ThreadNumber },
                i => {
                    this.fileDownloader.DownloadFileAsync(new Uri(urls[i]), config.PathToOutputDir, (int) (config.SpeedLimit / config.ThreadNumber)).Wait();
                });

        }
    }
}