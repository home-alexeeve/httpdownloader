﻿using Microsoft.Extensions.DependencyInjection;

namespace HttpFileDownloader {
    class Program {
        static void Main(string[] args) {
            var provider = IocBindings.Configure().BuildServiceProvider();

            provider.GetService<IConfigBuilder>().Validate(args);
            var cliApp = provider.GetService<ICliApp>().GetApp();
            cliApp.Execute(args);
        }
    }
}