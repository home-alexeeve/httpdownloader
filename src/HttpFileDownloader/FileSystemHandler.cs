using System.IO;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace HttpFileDownloader {
    public interface IFileSystemHandler {
        void WriteToFile(byte[] bytes, string path);

        void WriteToFile(Stream stream, string path, int speedLimit);

        string[] ReadTxt(string path);

        string ComposeLocalFilename(string destinationFolder, string destinationFileName);
    }

    public class FileSystemHandler : IFileSystemHandler {
        private ILogger logger;

        public FileSystemHandler(ILoggerFactory loggerFactory) {
            this.logger = loggerFactory.CreateLogger(this.GetType().Name);

        }

        public string[] ReadTxt(string path) {
            return System.IO.File.ReadAllLines(path);
        }

        public void WriteToFile(byte[] bytes, string path) {
            if (!Path.IsPathRooted(path)) {
                path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), path);
            }

            System.IO.Directory.CreateDirectory(Path.GetDirectoryName(path));
            logger.LogDebug("write stream to file " + path);
            File.WriteAllBytes(path, bytes);
        }

        public void WriteToFile(Stream stream, string path, int speedLimit) {
            if (!Path.IsPathRooted(path)) {
                path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), path);
            }

            using(var fileStream = new FileStream(path, FileMode.Create)) {
                var throtllingStream = speedLimit > 0 ? new ThrottledStream(fileStream, speedLimit) : new ThrottledStream(fileStream);
                byte[] buffer = new byte[1024];

                int read;
                int numBytesRead = 0;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
                    throtllingStream.Write(buffer, 0, read);
                    numBytesRead += read;
                }

                stream.Close();
                throtllingStream.Close();

            }
        }

        public string ComposeLocalFilename(string destinationFolder, string destinationFileName) {
            return Path.Combine(destinationFolder, destinationFileName);
        }
    }
}