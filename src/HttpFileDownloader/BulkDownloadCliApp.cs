using System.Linq;
using Microsoft.Extensions.CommandLineUtils;

namespace HttpFileDownloader {
    public interface ICliApp {
        CommandLineApplication GetApp();
    }

    public class BulkDownloadCliApp : ICliApp {
        private readonly IBulkFileDownloader bulkDownloader;
        private readonly IConfigBuilder configBuilder;
        private CommandLineApplication app = new CommandLineApplication(throwOnUnexpectedArg: false);
        private string command = ConfigurationNameLookup.ConfigDetails[ConfigurationNameEnum.BulkDownload];

        public BulkDownloadCliApp(
            IBulkFileDownloader bulkDownloader,
            IConfigBuilder configBuilder) {
            this.bulkDownloader = bulkDownloader;
            this.configBuilder = configBuilder;

            this.app = this.configBuilder.SetupCliOptions(command);
            this.app.Commands.FirstOrDefault(a => a.Name == command)?.OnExecute(() => {
                this.bulkDownloader.BulkDownload(configBuilder.BuildConfig(app.Commands.First(a => a.Name == command).Options));

                return 0;
            });
        }
        public CommandLineApplication GetApp() {
            return this.app;
        }
    }
}