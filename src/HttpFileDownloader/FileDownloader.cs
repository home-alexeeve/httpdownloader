using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace HttpFileDownloader {
    public interface IFileDownloader {
        Task DownloadFileAsync(Uri source, string folderName, int speedLimit);
    }

    public class FileDownloader : IFileDownloader {
        private readonly ILogger logger;
        private readonly IFileSystemHandler fileSystemHandler;
        private readonly IHttpClientFactory httpClientFactory;
        private IHttpClientService httpClient;
        public FileDownloader(
            ILoggerFactory loggerFactory,
            IFileSystemHandler fileSystemHandler,
            IHttpClientFactory httpClientFactory) {
            this.httpClientFactory = httpClientFactory;
            this.fileSystemHandler = fileSystemHandler;
            this.logger = loggerFactory.CreateLogger(this.GetType().Name);
        }

        public async Task DownloadFileAsync(Uri source, string folderName, int speedLimit) {
            this.logger.LogDebug("DownloadFileAsync({0}, {1}) is called.", source, folderName);
            this.httpClient = httpClientFactory.Create(source);
            var fileName = fileSystemHandler.ComposeLocalFilename(folderName, source?.Segments?.Last());

            Stopwatch sw = new Stopwatch();
            sw.Start();
            var fileContent = await GetHttpStream(source);

            logger.LogInformation($"Elapsed time for downloading {sw.Elapsed}");
            fileSystemHandler.WriteToFile(fileContent, fileName, speedLimit);
            sw.Stop();
        }

        private async Task<Stream> GetHttpStream(Uri source) {
            try {
                this.logger.LogDebug("Download started. Source: {0}", source);
                return await this.httpClient.DownloadFile(source.AbsoluteUri);
            } catch (Exception e) {
                this.logger.LogError("Download failed: {0}", e.Message);
                throw;
            }
        }

    }

}