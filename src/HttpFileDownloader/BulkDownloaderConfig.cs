using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.CommandLineUtils;

namespace HttpFileDownloader {
    public interface IConfigBuilder {
        void Validate(string[] args);
        CommandLineApplication SetupCliOptions(string configName);
        BulkDownloaderConfig BuildConfig(List<CommandOption> opts);
    }

    public class BulkDownloaderConfig : IConfigBuilder {
        public BulkDownloaderConfig() {
            ThreadNumber = 1;
            PathToOutputDir = string.Empty;
        }
        public string PathToFile { get; set; }
        public string PathToOutputDir { get; set; }
        public int ThreadNumber { get; set; }
        public int SpeedLimit { get; set; }
        public void Validate(string[] args) {

            if (args.Length == 0) {
                System.Console.WriteLine("Usage: HttpFileDownloader [option]");
                System.Console.WriteLine("Valid options:");
                System.Console.WriteLine("bulk_download [allow downloading files from the internet, concurrently]");
                System.Console.WriteLine("use -? for help");

                Environment.Exit(1);
            }

            if (args[0] != ConfigurationNameLookup.ConfigDetails[ConfigurationNameEnum.BulkDownload]) {
                Environment.Exit(1);
            }
        }
        public CommandLineApplication SetupCliOptions(string configName) {
            var app = new CommandLineApplication(throwOnUnexpectedArg: false);
            app.HelpOption("-? | -h | --help");

            app.Command(configName, c => {
                c.Description = "downloading files from the internet, concurrently and with a specified download speed";

                c.Option("-f|--file", "path to file with download URLs", CommandOptionType.SingleValue);
                c.Option("-o|--outputdir", "path to directory where downloaded files will be stored", CommandOptionType.SingleValue);
                c.Option("-n|--nthreads", "number of concurrent threads", CommandOptionType.SingleValue);
                c.Option("-l|--limitSpeed", "max limit of download speed in Kbytes per second", CommandOptionType.SingleValue);
            });

            return app;
        }
        public BulkDownloaderConfig BuildConfig(List<CommandOption> opts) {

            var pathTofileOpt = opts.First(o => o.ShortName == "f");
            var config = new BulkDownloaderConfig();

            if (pathTofileOpt.Value() != null && pathTofileOpt.Value() != string.Empty) {
                if (Path.IsPathRooted(pathTofileOpt.Value())) {
                    config.PathToFile = pathTofileOpt.Value();
                } else {
                    config.PathToFile = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\" + pathTofileOpt.Value();
                }
            } else {
                throw new ArgumentException("File name is not specified");
            }

            var pathToOutputDirOpt = opts.First(o => o.ShortName == "o");
            if (pathToOutputDirOpt.Value() != null && pathToOutputDirOpt.Value() != string.Empty) {
                config.PathToOutputDir = pathToOutputDirOpt.Value();
            }

            var threadNumberOpt = opts.First(o => o.ShortName == "n");
            if (threadNumberOpt.Value() != null && threadNumberOpt.Value() != string.Empty) {
                int threadsNum;
                int.TryParse(threadNumberOpt.Value(), out threadsNum);

                config.ThreadNumber = (threadsNum == 0) ? Environment.ProcessorCount * 2 : threadsNum;
            }

            var speedLimitOpt = opts.First(o => o.ShortName == "l");
            if (speedLimitOpt.Value() != null && speedLimitOpt.Value() != string.Empty) {
                int speedLimit;
                int.TryParse(speedLimitOpt.Value(), out speedLimit);

                config.SpeedLimit = speedLimit * 1024;
            }

            return config;
        }
    }
}