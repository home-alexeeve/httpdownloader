using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpFileDownloader {
    public static class HttpClientExtensions {
        public static async Task<Stream> GetAsync(
            this HttpClient client,
            string absoluteUri,
            IDictionary<string, string> headers = null) {
            return await client.SendAsync(absoluteUri, HttpMethod.Get, headers : headers);
        }

        private static async Task<Stream> SendAsync(
            this HttpClient client,
            string absoluteUri,
            HttpMethod method,
            object body = null,
            IDictionary<string, string> headers = null) {
            return await SendAsyncInternal(client, absoluteUri, method, body, headers);
        }

        private static async Task<Stream> SendAsyncInternal(
            this HttpClient client,
            string absoluteUri,
            HttpMethod method,
            object body = null,
            IDictionary<string, string> headers = null) {
            HttpContent objectBody = body as HttpContent;

            var msg = new HttpRequestMessage {
                RequestUri = new Uri(absoluteUri, UriKind.Absolute),
                Method = method,
                Content = objectBody
            };

            if (headers != null) {
                foreach (var header in headers) {
                    msg.Headers.Add(header.Key, header.Value);
                }
            }

            var message = await client.SendAsync(msg);

            if (message?.Content != null) {
                return await message.Content.ReadAsStreamAsync();
            }

            throw new Exception($"Error during reading content.  content lenght: {message.Content.Headers.ContentLength}");
        }
    }
}