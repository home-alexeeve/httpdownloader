using System.Collections.Generic;

namespace HttpFileDownloader {
    public static class ConfigurationNameLookup {

        public static readonly Dictionary<ConfigurationNameEnum, string> ConfigDetails = new Dictionary<ConfigurationNameEnum, string>() {
            {
            ConfigurationNameEnum.BulkDownload, "bulk_download"
            }
        };
    }

    public enum ConfigurationNameEnum {
        BulkDownload
    }
}