using System;
using System.Net.Http;
using System.Security.Authentication;

namespace HttpFileDownloader {
    public interface IHttpClientFactory {
        IHttpClientService Create(Uri endpoint, string accept = "image/*");
    }

    public class HttpClientInstance : IHttpClientFactory {
        private static IHttpClientService backendClient;

        public IHttpClientService Create(Uri endpoint, string accept = "image/*") {
            if (endpoint == null) {
                throw new ArgumentException("No endpoint specified");
            }

            if (backendClient == null) {

                backendClient = CreateClient(endpoint, accept);
            }

            return backendClient;
        }

        private static IHttpClientService CreateClient(Uri endpoint, string accept) {
            HttpClientHandler handler = new HttpClientHandler {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                SslProtocols = SslProtocols.Tls12
            };

            var httpClient = new HttpClient(handler) {
                BaseAddress = endpoint
            };

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(accept));

            return new HttpClientService(httpClient);
        }
    }
}