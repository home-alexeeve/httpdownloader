using System;
using System.Linq;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace HttpFileDownloader.Tests {
	public class BulkDownloaderTest {
		[Fact]
		public void BulkDownload_CommandLineApplication_ParseArguments_DefaultParams_Test() {
			var args = new string[] {
				"bulk_download",
				"-f",
				"urls.txt"
			};

			var provider = IocBindings.Configure().BuildServiceProvider();

			var cfgBuilder = provider.GetService<IConfigBuilder>();
			var app = cfgBuilder.SetupCliOptions(args[0]);
			app.Execute(args);

			var cfg = cfgBuilder.BuildConfig(app.Commands.First(cmd => cmd.Name == args[0]).Options);
			cfg.PathToFile.Should().EndWith(args[2], "Not expected path to file.");
			cfg.PathToOutputDir.Should().Be(string.Empty, "Not expected path to output dir.");
			cfg.ThreadNumber.Should().Be(1, "Not expected number of threads");
			cfg.SpeedLimit.Should().Be(0, "Not expected spped limit");
		}

		[Fact]
		public void BulkDownload_CommandLineApplication_ParseArguments_Test() {
			var args = new string[] {
				"bulk_download",
				"-f",
				"urls.txt",
				"-o",
				"path",
				"-n",
				"4",
				"-l",
				"1000"
			};
			var provider = IocBindings.Configure().BuildServiceProvider();

			var cfgBuilder = provider.GetService<IConfigBuilder>();
			var app = cfgBuilder.SetupCliOptions(args[0]);
			app.Execute(args);

			var cfg = cfgBuilder.BuildConfig(app.Commands.First(cmd => cmd.Name == args[0]).Options);
			cfg.PathToFile.Should().EndWith(args[2], "Not expected path to file.");
			cfg.PathToOutputDir.Should().EndWith(args[4], "Not expected path to output dir.");
			cfg.ThreadNumber.Should().Be(int.Parse(args[6]), "Not expected number of threads");
			cfg.SpeedLimit.Should().Be(int.Parse(args[8]) * 1024, "Not expected spped limit");
		}

		[Fact]
		public void BulkDownload_CommandLineApplication_ParseArguments_SourceFileNotSpecified_Test() {
			var args = new string[] {
				"bulk_download",
				"-o",
				"path",
				"-n",
				"4",
				"-l",
				"1000"
			};
			var provider = IocBindings.Configure().BuildServiceProvider();

			var cfgBuilder = (BulkDownloaderConfig) provider.GetService<IConfigBuilder>();
			var app = cfgBuilder.SetupCliOptions(args[0]);
			app.Execute(args);

			Action act = () => {
				cfgBuilder.BuildConfig(app.Commands.First(cmd => cmd.Name == args[0]).Options);
			};

			act.Should().Throw<ArgumentException>()
				.Where(e => e.Message.StartsWith("File name is not specified"));
		}

		[Fact]
		public void BulkDownload_HttpClientFactory_UrlIsNotSpecified_Test() {
			var factory = new HttpClientInstance();
			Action act = () => {
				factory.Create(null);
			};

			act.Should().Throw<ArgumentException>()
				.Where(e => e.Message.StartsWith("No endpoint specified"));
		}

		[Fact]
		public void BulkDownload_HttpClientFactory_Test() {
			var factory = new HttpClientInstance();
			var service = factory.Create(new Uri("http://sampleurl.com/image.jpg"));
			service.Should().NotBeNull().And.BeOfType<HttpClientService>();
		}
	}

}